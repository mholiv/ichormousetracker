# Mouse Tracker #

This is a sample application that I wrote for a client.

This application is a fair representation of my non-object oriented python code. This being said this application does use an ORM with SQLAlchemy.

I traditionally design REST API only web applications. This project caused me to realize I need to learn a javascript framework. I have since begun learning react. 

The web application does not protect against CSRF. If I were to implement CSRF, I would use a well-known library. Something like CsrfProtect.

Since learning react.js the HTML/javascript sections of this project do not reflect my current ability. 

If this where an enterprise application there would be unit test file and CI via Travis. As the client did not want these... well they were not included.

Given the trivial nature, single developer workflow, and expectation that this repository ould not be made public, all development was done in the master branch. In any application other than this I wouls use the gitflow workflow. (i.e. dev branch with feature branches merging into master as releases) 


### What is this repository for? ###

the purpose is to set up a lab mouse tracking system.

### How do I get set up? ###

This application is a standard uwsgi python application.

You can launch it either via standard uwsgi means nor my launching the `mouse.py` file

The dependencies are listed in the `requirements.txt` file. This application also needs a Postgres server.