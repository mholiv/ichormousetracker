from sqlalchemy import Column, Integer, String, MetaData, Table, create_engine, ForeignKey, DateTime, Boolean
from sqlalchemy.orm import sessionmaker, relationship
from sqlalchemy.ext.declarative import declarative_base
from settings import *


metadata = MetaData()
Base = declarative_base()


# ------------- Define Tables -------------
cages = Table('cages', metadata,
    Column('cage_id', Integer, primary_key=True),
    Column('name', String(), nullable=False, unique=True)
)

color = Table('colors', metadata,
    Column('color_id', Integer, primary_key=True),
    Column('color', String(), nullable=False, unique=True),
)

sexes = Table('sexes', metadata,
    Column('sex_id', Integer, primary_key=True),
    Column('sex', String(), nullable=False, unique=True),
)

diets = Table('diets', metadata,
    Column('diet_id', Integer, primary_key=True),
    Column('diet', String(), nullable=False, unique=True),
)

statuses = Table('statuses', metadata,
    Column('status_id', Integer, primary_key=True),
    Column('status', String(), nullable=False, unique=True),
)

studies = Table('studies', metadata,
    Column('study_id', Integer, primary_key=True),
    Column('study', String(), nullable=False, unique=True),
)

strains = Table('strains', metadata,
    Column('strain_id', Integer, primary_key=True),
    Column('strain', String(), nullable=False, unique=True),
)

breeds = Table('breeds', metadata,
    Column('breed_id', Integer, primary_key=True),
    Column('breed', String(), nullable=False, unique=True),
)

pools = Table('pools', metadata,
    Column('pool_id', Integer, primary_key=True),
    Column('pool', String(), nullable=False, unique=True),
)

causes = Table('causes', metadata,
    Column('cause_id', Integer, primary_key=True),
    Column('cause', String(), nullable=False, unique=True),
)

mice = Table('mice',metadata,
    Column('mouse_id',Integer, primary_key=True),
    Column('parent1', Integer),
    Column('parent2', Integer),
    Column('tag_number', String()),
    Column('birth_date', DateTime),
    Column('death_date', DateTime),
    Column('death_note', String()),
    Column('general_note', String()),
    Column('_cage_id', Integer, ForeignKey("cages.cage_id")),
    Column('_color_id', Integer, ForeignKey("colors.color_id")),
    Column('_sex_id', Integer, ForeignKey("sexes.sex_id")),
    Column('_diet_id', Integer, ForeignKey("diets.diet_id")),
    Column('_status_id', Integer, ForeignKey("statuses.status_id")),
    Column('_study_id', Integer, ForeignKey("studies.study_id")),
    Column('_strain_id', Integer, ForeignKey("strains.strain_id")),
    Column('_breed_id', Integer, ForeignKey("breeds.breed_id")),
    Column('_pool_id', Integer, ForeignKey("pools.pool_id")),
    Column('_cause_id', Integer, ForeignKey("causes.cause_id"))
)

weight_histories = Table('weight_histories', metadata,
    Column('weight_history_id', Integer, primary_key=True),
    Column('_mouse_id', Integer, ForeignKey("mice.mouse_id"), nullable=False),
    Column('weight', Integer, nullable=False),
    Column('date', DateTime, nullable=False)
)

gather_methods = Table('gather_methods', metadata,
    Column('gather_method_id', Integer, primary_key=True),
    Column('gather_method', String(), nullable=False, unique=True),
)

gather_hows = Table('gather_hows', metadata,
    Column('gather_how_id', Integer, primary_key=True),
    Column('gather_how', String(), nullable=False, unique=True),
)

blood_histories = Table('blood_histories', metadata,
    Column('blood_histories_id', Integer, primary_key=True),
    Column('_mouse_id', Integer, ForeignKey("mice.mouse_id"), nullable=False),
    Column('date', DateTime, nullable=False),
    Column('_gather_method_id', Integer, ForeignKey("gather_methods.gather_method_id"), nullable=False),
    Column('_gather_how_id', Integer, ForeignKey("gather_hows.gather_how_id"), nullable=False),
    Column('outcome', String(), nullable=False),
    Column('notes', String())
)

surgeons = Table('surgeons', metadata,
    Column('surgeon_id', Integer, primary_key=True),
    Column('surgeon', String(), nullable=False, unique=True),
 )

surgical_plans = Table('surgical_plans', metadata,
    Column('surgical_plan_id', Integer, primary_key=True),
    Column('_surgeon_id', Integer, ForeignKey("surgeons.surgeon_id"), nullable=False),
    Column('date', DateTime, nullable=False),
    Column('procedure', String(), nullable=False),
    Column('time', String()),
    Column('patient', String()),
    Column('weight', Integer),
    Column('exam_notes', String()),
    Column('exam_description', String()),
    Column('sedation', String()),
    Column('plane', String()),
    Column('blade', String()),
    Column('suture', String()),
    Column('forceps', String()),
    Column('syringe', String()),
    Column('scissors', String()),
    Column('surgical_meloxicam', Boolean),
    Column('surgical_lidocaine', Boolean),
    Column('surgical_proparicaine', Boolean),
    Column('surgical_neo_poly_bac_ophthalmic', Boolean),
    Column('surgical_dose', String()),
    Column('post_meloxicam', Boolean),
    Column('post_lidocaine', Boolean),
    Column('post_proparicaine', Boolean),
    Column('post_neo_poly_bac_ophthalmic', Boolean),
    Column('post_dose', String()),
    Column('suture_removed', Boolean),
    Column('suture_removal_date', DateTime),
    Column('heat_lamp', Boolean),
    Column('sq_fluids', Boolean),
    Column('ip_fluids', Boolean),
    Column('approved', String()),
    Column('approval_date', DateTime),
)


# ------------- Define Classes -------------

class Surgical_Plan(Base):
    __tablename__ = 'surgical_plans'
    surgical_plan_id = Column(Integer, primary_key=True)
    _surgeon_id = Column(Integer, ForeignKey("surgeons.surgeon_id"))
    date = Column(DateTime, nullable=False)
    procedure = Column(String(), nullable=False)
    time = Column(String())
    patient = Column(String())
    weight = Column(Integer)
    exam_notes = Column(String())
    exam_description = Column(String())
    sedation = Column(String())
    plane = Column(String())
    blade = Column(String())
    suture = Column(String())
    forceps = Column(String())
    syringe = Column(String())
    scissors = Column(String())
    surgical_meloxicam = Column(Boolean)
    surgical_lidocaine = Column(Boolean)
    surgical_proparicaine = Column(Boolean)
    surgical_neo_poly_bac_ophthalmic = Column(Boolean)
    surgical_dose = Column(String())
    post_meloxicam = Column(Boolean)
    post_lidocaine = Column(Boolean)
    post_proparicaine = Column(Boolean)
    post_neo_poly_bac_ophthalmic = Column(Boolean)
    post_dose = Column(String())
    suture_removed = Column(Boolean)
    suture_removal_date = Column(DateTime)
    heat_lamp = Column(Boolean)
    sq_fluids = Column(Boolean)
    ip_fluids = Column(Boolean)
    approved = Column(String())
    approval_date = Column(DateTime)


class Surgeon(Base):
    __tablename__ = 'surgeons'
    surgeon_id = Column(Integer, primary_key=True)
    surgeon = Column(String())


class Blood_Event(Base):
    __tablename__ = 'blood_histories'
    blood_event_id = Column(Integer, primary_key=True)
    _mouse_id = Column(Integer, ForeignKey("mice.mouse_id"))
    event_date = Column(DateTime)
    _gather_method_id = Column(Integer, ForeignKey("gather_methods.gather_method_id"))
    _gather_how_id = Column(Integer, ForeignKey("gather_hows.gather_how_id"))
    outcome = Column(String())
    notes = Column(String())

    def __repr__(self):
        return "<Blood Event (id='%s', mouse_id='%s')>" % (self.blood_event_id, self._mouse_id)


class Gather_How(Base):
    __tablename__ = 'gather_hows'
    gather_how_id = Column(Integer, primary_key=True)
    gather_how = Column(String(), unique=True)

    def __repr__(self):
        return "<Gather how (id='%s', how='%s')>" % (self.gather_how_id, self.gather_how)


class Gather_Method(Base):
    __tablename__ = 'gather_methods'
    gather_method_id = Column(Integer, primary_key=True)
    gather_method = Column(String(), unique=True)

    def __repr__(self):
        return "<Gather method (id='%s', method='%s')>" % (self.gather_method_id, self.gather_method)


class Weight_Event(Base):
    __tablename__ = 'weight_histories'
    weight_event_id = Column(Integer, primary_key=True)
    _mouse_id = Column(Integer, ForeignKey("mice.mouse_id"))
    weight = Column(Integer)
    event_date = Column(DateTime)

    def __repr__(self):
        return "<Weight Event (id='%s', mouse_id='%s' weight='%s g')>" % (self.weight_event_id, self._mouse_id, self.weight)


class Mouse(Base):
    __tablename__ = 'mice'
    mouse_id = Column(Integer, primary_key=True)
    parent1 = Column(Integer)
    parent2 = Column(Integer)
    tag_number = Column(String)
    birth_date = Column(DateTime)
    death_date = Column(DateTime)
    death_note = Column(String())
    general_note = Column(String())
    _cage_id = Column(Integer, ForeignKey("cages.cage_id"))
    _color_id = Column(Integer, ForeignKey("colors.color_id"))
    _sex_id = Column(Integer, ForeignKey("sexes.sex_id"))
    _diet_id = Column(Integer, ForeignKey("diets.diet_id"))
    _status_id = Column(Integer, ForeignKey("statuses.status_id"))
    _study_id = Column(Integer, ForeignKey("studies.study_id"))
    _strain_id = Column(Integer, ForeignKey("strains.strain_id"))
    _breed_id = Column(Integer, ForeignKey("breeds.breed_id"))
    _pool_id = Column(Integer, ForeignKey("pools.pool_id"))
    _cause_id = Column(Integer, ForeignKey("causes.cause_id"))
    cage = relationship("Cage", foreign_keys=[_cage_id])
    color = relationship("Color", foreign_keys=[_color_id])
    sex = relationship("Sex", foreign_keys=[_sex_id])
    diet = relationship("Diet", foreign_keys=[_diet_id])
    status = relationship("Status", foreign_keys=[_status_id])
    strain = relationship("Strain", foreign_keys=[_strain_id])
    study = relationship("Study", foreign_keys=[_study_id])
    breed = relationship("Breed", foreign_keys=[_breed_id])
    pool = relationship("Pool", foreign_keys=[_pool_id])
    cause = relationship("Cause", foreign_keys=[_cause_id])

    def strain_str(self):
        if self.strain is not None:
            return self.strain.strain
        else:
            return ''

    def study_str(self):
        if self.study is not None:
            return self.study.study
        else:
            return ''

    def sex_str(self):
        if self.sex is not None:
            return self.sex.sex
        else:
            return ''

    def death_date_str(self):
        if self.death_date is not None:
            return self.death_date.strftime('%m/%d/%Y')
        else:
            return ''

    def birth_date_str(self):
        if self.birth_date is not None:
            return self.birth_date.strftime('%m/%d/%Y')
        else:
            return ''

    def cage_sort_id(self):
        if self.cage is not None:
            return self.cage.name
        else:
            return ''


    def __repr__(self):
        return "<Mouse (id='%s', tag='%s')>" % (self.mouse_id, self.tag_number)


class Cage(Base):
    __tablename__ = 'cages'
    cage_id = Column(Integer, primary_key=True)
    name = Column(String, unique=True)

    def __repr__(self):
        return "<Cage (id='%s', name='%s')>" % (self.cage_id, self.name)


class Color(Base):
    __tablename__ = 'colors'
    color_id = Column(Integer, primary_key=True)
    color = Column(String, unique=True)

    def __repr__(self):
        return "<Color (id='%s', color='%s')>" % (self.color_id, self.color)


class Sex(Base):
    __tablename__ = 'sexes'
    sex_id = Column(Integer, primary_key=True)
    sex = Column(String, unique=True)

    def __repr__(self):
        return "<Sex (id='%s', sex='%s')>" % (self.sex_id, self.sex)


class Diet(Base):
    __tablename__ = 'diets'
    diet_id = Column(Integer, primary_key=True)
    diet = Column(String, unique=True)

    def __repr__(self):
        return "<Diet (id='%s', diet='%s')>" % (self.diet_id, self.diet)


class Status(Base):
    __tablename__ = 'statuses'
    status_id = Column(Integer, primary_key=True)
    status = Column(String, unique=True)

    def __repr__(self):
        return "<Status (id='%s', stat='%s')>" % (self.status_id, self.status)


class Study(Base):
    __tablename__ = 'studies'
    study_id = Column(Integer, primary_key=True)
    study = Column(String, unique=True)

    def __repr__(self):
        return "<Study (id='%s', study='%s')>" % (self.study_id, self.study)


class Strain(Base):
    __tablename__ = 'strains'
    strain_id = Column(Integer, primary_key=True)
    strain = Column(String, unique=True)

    def __repr__(self):
        return "<Strain (id='%s', strain='%s')>" % (self.strain_id, self.strain)


class Breed(Base):
    __tablename__ = 'breeds'
    breed_id = Column(Integer, primary_key=True)
    breed = Column(String, unique=True)

    def __repr__(self):
        return "<Breed (id='%s', breed='%s')>" % (self.breed_id, self.breed)


class Pool(Base):
    __tablename__ = 'pools'
    pool_id = Column(Integer, primary_key=True)
    pool = Column(String, unique=True)

    def __repr__(self):
        return "<Pool (id='%s', pool='%s')>" % (self.pool_id, self.pool)


class Cause(Base):
    __tablename__ = 'causes'
    cause_id = Column(Integer, primary_key=True)
    cause = Column(String, unique=True)

    def __repr__(self):
        return "<Cause (id='%s', cause='%s')>" % (self.cause_id, self.cause)

engine = create_engine('postgresql+psycopg2cffi://%s:%s@%s:%s/%s'% (settings_sql_username, settings_sql_password, settings_sql_dblocation, settings_sql_dbport, settings_sql_dbname))
if settings_create_database_if_not_present:
    metadata.create_all(engine)

Session = sessionmaker()
Session.configure(bind=engine)
alcsession = Session()

# qUser = alcsession.query(Company).filter(Company.email == 'ff').one()
# print(qUser.email,qUser.accountType.password)
