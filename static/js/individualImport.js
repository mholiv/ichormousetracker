$(document).ready(function() {


    // Top Menu Stuff
    var d = document.getElementById("individualImport");
    d.className += " acticeTopMenu";


    // Date picker for death date
    $( "#death_date" ).datepicker();


    // Date picker for birth date
    $( "#birth_date" ).datepicker();


    // Get cages dynamically
    $("#cage-tags").focus(function () {
        $.getJSON('/internal/cages', function (result) {
            $("#cage-tags").autocomplete({
              source: result
            });
        });
    });



    // Get sexes dynamically
    $("#sex-tags").focus(function () {
        $.getJSON('/internal/sexes', function (result) {
            $("#sex-tags").autocomplete({
              source: result
            });
        });
    });


    // Get colors dynamically
    $("#color-tags").focus(function () {
        $.getJSON('/internal/colors', function (result) {
            $("#color-tags").autocomplete({
              source: result
            });
        });
    });


    // Get diets dynamically
    $("#diet-tags").focus(function () {
        $.getJSON('/internal/diets', function (result) {
            $("#diet-tags").autocomplete({
              source: result
            });
        });
    });


    // Get strain dynamically
    $("#strain-tags").focus(function () {
        $.getJSON('/internal/strains', function (result) {

            availableTags  = result

            $("#strain-tags").autocomplete({
              source: result
            });
        });
    });



    // Get breed dynamically
    $("#breed-tags").focus(function () {
        $.getJSON('/internal/breeds', function (result) {
            $("#breed-tags").autocomplete({
              source: result
            });
        });
    });


    // Get status dynamically
    $("#status-tags").focus(function () {
        $.getJSON('/internal/statuses', function (result) {
            $("#status-tags").autocomplete({
              source: result
            });
        });
    });


    // Get studies dynamically
    $("#study-tags").focus(function () {
        $.getJSON('/internal/studies', function (result) {
            $("#study-tags").autocomplete({
              source: result
            });
        });
    });


    // Get pool dynamically
    $("#pool-tags").focus(function () {
        $.getJSON('/internal/pools', function (result) {
            $("#pool-tags").autocomplete({
              source: result
            });
        });
    });


    // Get death causes dynamically
    $("#cause-tags").focus(function () {
        $.getJSON('/internal/causes_of_death', function (result) {
            $("#cause-tags").autocomplete({
              source: result
            });
        });
    });


    //Submit button
    $("#submit_button").click(function () {
        var formData = JSON.stringify($("#mouse_form").serializeArray());

        $.ajax({
          type: "POST",
          url: "/internal/add_mouse",
          data: formData,
          success: function(data){
              if(data['status'] == 'ok'){
                  //Do something to say all good
                  //Then clear the fields

                  $( "#mouse_add_error_loc" ).empty();
                  $("#mouse_add_error_loc").append("<H3>Mouse Added</H3>");
                  $('#mouse_form').find("input[type=text], textarea").val("");

              } else if (data['status'] == 'error'){
                  //Do something to list the errors
                  $( "#mouse_add_error_loc" ).empty();
                  $("#mouse_add_error_loc").append("<ul id='mice_add_errors'>");
                  $.each(data['errors'], function(i, error){
                      $("#mouse_add_error_loc").append("<li>"+error +"</li>");
                  });
                  $("#mouse_add_error_loc").append("</ul>");

              } else {
                  alert("Server error 1: Try again. If issue persists contact server admin.");
              }
          },
          dataType: "json",
          contentType : "application/json",
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                alert("Server error 2: Try again. If issue persists contact server admin. Details: "+XMLHttpRequest+textStatus+errorThrown);
            },
        });
    });

});

