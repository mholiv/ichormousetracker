if(window.attachEvent) {
    window.attachEvent('onload', loader);
} else {
    if(window.onload) {
        var curronload = window.onload;
        var newonload = function(evt) {
            curronload(evt);
            yourFunctionName(evt);
        };
        window.onload = newonload;
    } else {
        window.onload = loader;
    }
}

function loader() {
    var d = document.getElementById("bulkImport")
    d.className += " acticeTopMenu";
}