$(document).ready(function() {
    // Top Menu Stuff
    var d = document.getElementById("cageReport");
    d.className += " acticeTopMenu";


    // Keep track of hidden search
    var current_search = 1;

    // Generate autofilled content
    $(".search_value_objects").focus(function () {

        var ident_number = event.target.id;
        var ident_name = `#criteria${ident_number}`;
        var search_category = $(ident_name).val();

        $.getJSON('/internal/'+search_category, function (result) {
            $("#"+ident_number).autocomplete({
              source: result
            });
        });
    });
    
    
    //Unhide next item
    $("#add_search_constraint").click(function () {

        var next_to_unhide = `#search${current_search}`;
        $(next_to_unhide).show();
        current_search++
    });
    
    
    //Submit button
    $("#submit_button").click(function () {
        var formData = JSON.stringify($("#mouse_form").serializeArray());
    });

});