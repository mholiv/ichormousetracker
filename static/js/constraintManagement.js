if(window.attachEvent) {
    window.attachEvent('onload', loader);
} else {
    if(window.onload) {
        var curronload = window.onload;
        var newonload = function(evt) {
            curronload(evt);
            yourFunctionName(evt);
        };
        window.onload = newonload;
    } else {
        window.onload = loader;
    }
}

function loader() {
    var acc = document.getElementsByClassName('accordion');
    var i;
    for (i = 0; i < acc.length; i++) {
        acc[i].onclick = function(){
            this.classList.toggle("active");
            this.nextElementSibling.classList.toggle("show");
      }
    }

    var d = document.getElementById("constraintManagement")
    d.className += " acticeTopMenu";
}

$(document).ready(function(){
    $("#get_cage").click(function(){
        $.getJSON("/internal/cages", function(result){
            $("#cage_list_loc").append("<ul>");
            $.each(result, function(i, field){
                $("#cage_list_loc").append("<li>"+field +"</li>");
            });
            $("#cage_list_loc").append("</ul>");
        });
    });
});

$(document).ready(function(){
    $("#get_color").click(function(){
        $.getJSON("/internal/colors", function(result){
            $("#color_list_loc").append("<ul>");
            $.each(result, function(i, field){
                $("#color_list_loc").append("<li>"+field +"</li>");
            });
            $("#color_list_loc").append("</ul>");
        });
    });
});


$(document).ready(function(){
    $("#get_sex").click(function(){
        $.getJSON("/internal/sexes", function(result){
            $("#sex_list_loc").append("<ul>");
            $.each(result, function(i, field){
                $("#sex_list_loc").append("<li>"+field+"</li>");
            });
            $("#sex_list_loc").append("</ul>");
        });
    });
});


$(document).ready(function(){
    $("#get_diet").click(function(){
        $.getJSON("/internal/diets", function(result){
            $("#diet_list_loc").append("<ul>");
            $.each(result, function(i, field){
                $("#diet_list_loc").append("<li>"+field+"</li>");
            });
            $("#diet_list_loc").append("</ul>");
        });
    });
});


$(document).ready(function(){
    $("#get_status").click(function(){
        $.getJSON("/internal/statuses", function(result){
            $("#status_list_loc").append("<ul>");
            $.each(result, function(i, field){
                $("#status_list_loc").append("<li>"+field+"</li>");
            });
            $("#status_list_loc").append("</ul>");
        });
    });
});


$(document).ready(function(){
    $("#get_studies").click(function(){
        $.getJSON("/internal/studies", function(result){
            $("#study_list_loc").append("<ul>");
            $.each(result, function(i, field){
                $("#study_list_loc").append("<li>"+field+"</li>");
            });
            $("#study_list_loc").append("</ul>");
        });
    });
});


$(document).ready(function(){
    $("#get_strain").click(function(){
        $.getJSON("/internal/strains", function(result){
            $("#strain_list_loc").append("<ul>");
            $.each(result, function(i, field){
                $("#strain_list_loc").append("<li>"+field+"</li>");
            });
            $("#strain_list_loc").append("</ul>");
        });
    });
});


$(document).ready(function(){
    $("#get_breed").click(function(){
        $.getJSON("/internal/breeds", function(result){
            $("#breed_list_loc").append("<ul>");
            $.each(result, function(i, field){
                $("#breed_list_loc").append("<li>"+field+"</li>");
            });
            $("#breed_list_loc").append("</ul>");
        });
    });
});


$(document).ready(function(){
    $("#get_pool").click(function(){
        $.getJSON("/internal/pools", function(result){
            $("#pool_list_loc").append("<ul>");
            $.each(result, function(i, field){
                $("#pool_list_loc").append("<li>"+field+"</li>");
            });
            $("#pool_list_loc").append("</ul>");
        });
    });
});


$(document).ready(function(){
    $("#get_cause_of_death").click(function(){
        $.getJSON("/internal/causes_of_death", function(result){
            $("#cause_of_death_list_loc").append("<ul>");
            $.each(result, function(i, field){
                $("#cause_of_death_list_loc").append("<li>"+field+"</li>");
            });
            $("#cause_of_death_list_loc").append("</ul>");
        });
    });
});


$(document).ready(function(){
    $("#get_blood_gather_method").click(function(){
        $.getJSON("/internal/blood_gather_methods", function(result){
            $("#blood_gather_method_list_loc").append("<ul>");
            $.each(result, function(i, field){
                $("#blood_gather_method_list_loc").append("<li>"+field+"</li>");
            });
            $("#blood_gather_method_list_loc").append("</ul>");
        });
    });
});


$(document).ready(function(){
    $("#get_blood_how").click(function(){
        $.getJSON("/internal/blood_hows", function(result){
            $("#blood_how_list_loc").append("<ul>");
            $.each(result, function(i, field){
                $("#blood_how_list_loc").append("<li>"+field+"</li>");
            });
            $("#blood_how_list_loc").append("</ul>");
        });
    });
});

$(document).ready(function(){
    $("#get_surgons").click(function(){
        $.getJSON("/internal/surgeons", function(result){
            $("#surgeon_list_loc").append("<ul>");
            $.each(result, function(i, field){
                $("#surgeon_list_loc").append("<li>"+field+"</li>");
            });
            $("#surgeon_list_loc").append("</ul>");
        });
    });
});