from flask import Flask
from flask import render_template
from flask import session
from flask import redirect
from flask import url_for
from flask import request
from flask import flash
from flask import Response

import requests

from settings import *
from internals import get_all_cages, add_cage_with_name
from internals import get_all_colors, add_color
from internals import get_all_sexes, add_sex
from internals import get_all_diets, add_diet
from internals import get_all_statuses, add_status
from internals import get_all_strains, add_strain
from internals import get_all_studies, add_study
from internals import get_all_breeds, add_breed
from internals import get_all_pools, add_pool
from internals import get_all_causes_of_death, add_cause_of_death
from internals import get_all_blood_gather_methods, add_blood_gather_method
from internals import get_all_blood_hows, add_blood_how
from internals import get_all_surgeons, add_surgeon
from internals import add_mouse, find_mice, get_mouse, update_mouse
from internals import generate_cage_card_from_mouse_blob

app = Flask(__name__)


@app.route("/logout")
def logout():
    del session['username']
    return redirect(url_for('login'))


@app.route("/login", methods=['GET', 'POST'])
def login():
    if request.method == 'POST':

        username = request.form['username']
        password = request.form['password']


        username = 'caitlin.campbell@ichortherapeutics.com'
        password = 'LL8c7gKvFMIDwJg'

        session['username'] = username
        return redirect(url_for('home'))

        if 'ichortherapeutics.com' in username:
            if username[-21:] == 'ichortherapeutics.com':

                test_url = 'https://outlook.office365.com/api/v1.0/Me/Calendars'
                response = requests.get(test_url, auth=(username, password))

                if response.status_code == 200:
                    session['username'] = username
                    return redirect(url_for('home'))
                else:
                    flash('Error Logging In. Error Code: %s' % response.status_code)
            else:
                flash('Error Logging In. Error Code: %s' % 403)
        else:
            flash('Error Logging In. Error Code: %s' % 403)
        return render_template('login.html')
    else:
        if 'username' in session:
            return redirect(url_for('home'))
        else:
            return render_template('login.html')


@app.route("/constraintManagement")
def constraint_management():
    if 'username' in session:
        return render_template('constraintManagement.html')
    else:
        return redirect(url_for('login'))


@app.route("/cageReport", methods=['GET', 'POST'])
def cageReport():
    if 'username' in session:
        if request.method == 'POST':

            criteria = []

            # We loop threw the form data and spit out a list of dics
            for i in range(5):
                current = str(i)
                if request.form['value'+current] == '':
                    pass
                else:
                    criteria.append({'criteria':request.form['criteria'+current],'value':request.form['value'+current]})

            mouse_blob = find_mice(criteria)

            mouse_blob.sort(key=lambda mouse: mouse.cage_sort_id())

            cage_cards = generate_cage_card_from_mouse_blob(mouse_blob)

            print(cage_cards)

            return render_template('cageReport.html', cards=cage_cards)

        else:
            return render_template('cageReport.html', cards = {})
    else:
        return redirect(url_for('login'))


@app.route("/summaryReport", methods=['GET', 'POST'])
def summaryReport():
    if 'username' in session:
        if request.method == 'POST':

            criteria = []

            # We loop threw the form data and spit out a list of dics
            for i in range(5):
                current = str(i)
                if request.form['value'+current] == '':
                    pass
                else:
                    criteria.append({'criteria':request.form['criteria'+current],'value':request.form['value'+current]})

            mouse_blob = find_mice(criteria)

            mouse_blob.sort(key=lambda mouse: mouse.cage_sort_id())

            return render_template('summaryReport.html', mice=mouse_blob)

        else:
            return render_template('summaryReport.html', mice = [])
    else:
        return redirect(url_for('login'))


@app.route("/search", methods=['GET', 'POST'])
def search():
    if 'username' in session:
        if request.method == 'POST':

            criteria = []

            # We loop threw the form data and spit out a list of dics
            for i in range(5):
                current = str(i)
                if request.form['value'+current] == '':
                    pass
                else:
                    criteria.append({'criteria':request.form['criteria'+current],'value':request.form['value'+current]})

            mouse_blob = find_mice(criteria)
            return render_template('search.html', mice=mouse_blob)

        else:
            return render_template('search.html', mice = [])
    else:
        return redirect(url_for('login'))


@app.route("/individualImport")
def individual_import():
    if 'username' in session:
        return render_template('individualImport.html')
    else:
        return redirect(url_for('login'))


@app.route("/bulkImport")
def bulk_import():
    if 'username' in session:
        return render_template('bulkImport.html')
    else:
        return redirect(url_for('login'))


@app.route("/mouse/<int:mouse_id>")
def mouse_details(mouse_id):
    if 'username' in session:
        mouse = get_mouse(mouse_id)
        return render_template('mousedetails.html', mouse=mouse)
    else:
        return redirect(url_for('login'))


@app.route("/")
def home():
    return redirect(url_for('search'))


@app.route("/internal/cages", methods=['GET', 'POST'])
def internal_cages():
    if 'username' in session:
        if request.method == 'POST':
            new_cage_name = request.form['cage_name']
            add_cage_with_name(new_cage_name)
            flash('Cage Added')
            return redirect(url_for('constraint_management'))
        else:
            resp = Response(response=get_all_cages(), status=200, mimetype="application/json")
            return resp
    else:
        return '403', 403


@app.route("/internal/colors", methods=['GET', 'POST'])
def internal_colors():
    if 'username' in session:
        if request.method == 'POST':
            new_color_name = request.form['color_name']
            add_color(new_color_name)
            flash('Color Added')
            return redirect(url_for('constraint_management'))
        else:
            resp = Response(response=get_all_colors(), status=200, mimetype="application/json")
            return resp
    else:
        return '403', 403


@app.route("/internal/sexes", methods=['GET', 'POST'])
def internal_sexes():
    if 'username' in session:
        if request.method == 'POST':
            new_sex_name = request.form['sex_name']
            add_sex(new_sex_name)
            flash('Sex Added')
            return redirect(url_for('constraint_management'))
        else:
            resp = Response(response=get_all_sexes(), status=200, mimetype="application/json")
            return resp
    else:
        return '403', 403


@app.route("/internal/diets", methods=['GET', 'POST'])
def internal_diets():
    if 'username' in session:
        if request.method == 'POST':
            new_diet_name = request.form['diet_name']
            add_diet(new_diet_name)
            flash('Diet Added')
            return redirect(url_for('constraint_management'))
        else:
            resp = Response(response=get_all_diets(), status=200, mimetype="application/json")
            return resp
    else:
        return '403', 403


@app.route("/internal/statuses", methods=['GET', 'POST'])
def internal_statuses():
    if 'username' in session:
        if request.method == 'POST':
            new_status_name = request.form['status_name']
            add_status(new_status_name)
            flash('Status Added')
            return redirect(url_for('constraint_management'))
        else:
            resp = Response(response=get_all_statuses(), status=200, mimetype="application/json")
            return resp
    else:
        return '403', 403


@app.route("/internal/studies", methods=['GET', 'POST'])
def internal_studies():
    if 'username' in session:
        if request.method == 'POST':
            new_study_name = request.form['study_name']
            add_study(new_study_name)
            flash('Study Added')
            return redirect(url_for('constraint_management'))
        else:
            resp = Response(response=get_all_studies(), status=200, mimetype="application/json")
            return resp
    else:
        return '403', 403


@app.route("/internal/strains", methods=['GET', 'POST'])
def internal_strains():
    if 'username' in session:
        if request.method == 'POST':
            new_strain_name = request.form['strain_name']
            add_strain(new_strain_name)
            flash('Strain Added')
            return redirect(url_for('constraint_management'))
        else:
            resp = Response(response=get_all_strains(), status=200, mimetype="application/json")
            return resp
    else:
        return '403', 403


@app.route("/internal/breeds", methods=['GET', 'POST'])
def internal_breeds():
    if 'username' in session:
        if request.method == 'POST':
            new_breed_name = request.form['breed_name']
            add_breed(new_breed_name)
            flash('Breed Added')
            return redirect(url_for('constraint_management'))
        else:
            resp = Response(response=get_all_breeds(), status=200, mimetype="application/json")
            return resp
    else:
        return '403', 403


@app.route("/internal/pools", methods=['GET', 'POST'])
def internal_pools():
    if 'username' in session:
        if request.method == 'POST':
            new_pool_name = request.form['pool_name']
            add_pool(new_pool_name)
            flash('Pool Added')
            return redirect(url_for('constraint_management'))
        else:
            resp = Response(response=get_all_pools(), status=200, mimetype="application/json")
            return resp
    else:
        return '403', 403


@app.route("/internal/causes_of_death", methods=['GET', 'POST'])
def internal_causes_of_death():
    if 'username' in session:
        if request.method == 'POST':
            new_cause_of_death_name = request.form['cause_of_death_name']
            add_cause_of_death(new_cause_of_death_name)
            flash('Cause of Death Added')
            return redirect(url_for('constraint_management'))
        else:
            resp = Response(response=get_all_causes_of_death(), status=200, mimetype="application/json")
            return resp
    else:
        return '403', 403


@app.route("/internal/blood_gather_methods", methods=['GET', 'POST'])
def internal_blood_gather_methods():
    if 'username' in session:
        if request.method == 'POST':
            new_blood_gather_method_name = request.form['blood_gather_method_name']
            add_blood_gather_method(new_blood_gather_method_name)
            flash('Blood Gather Method Added')
            return redirect(url_for('constraint_management'))
        else:
            resp = Response(response=get_all_blood_gather_methods(), status=200, mimetype="application/json")
            return resp
    else:
        return '403', 403


@app.route("/internal/blood_hows", methods=['GET', 'POST'])
def internal_blood_hows():
    if 'username' in session:
        if request.method == 'POST':
            new_blood_how_name = request.form['blood_how_name']
            add_blood_how(new_blood_how_name)
            flash('Blood How Added')
            return redirect(url_for('constraint_management'))
        else:
            resp = Response(response=get_all_blood_hows(), status=200, mimetype="application/json")
            return resp
    else:
        return '403', 403

@app.route("/internal/surgeons", methods=['GET', 'POST'])
def internal_surgeons():
    if 'username' in session:
        if request.method == 'POST':
            new_surgeon_name = request.form['surgeon_name']
            add_surgeon(new_surgeon_name)
            flash('Surgeon Added')
            return redirect(url_for('constraint_management'))
        else:
            resp = Response(response=get_all_surgeons(), status=200, mimetype="application/json")
            return resp
    else:
        return '403', 403


@app.route("/internal/add_mouse", methods=['GET', 'POST'])
def internal_add_mouse():
    if 'username' in session:
        if request.method == 'POST':
            content = request.get_json(silent=True)
            mesg = add_mouse(content)
            resp = Response(response=mesg, status=200, mimetype="application/json")
            return resp
        else:
            resp = Response(response=get_all_blood_hows(), status=200, mimetype="application/json")
            return resp
    else:
        return '403', 403


@app.route("/internal/update_mouse/<int:mouse_id>", methods=['GET', 'POST'])
def internal_update_mouse(mouse_id):
    if 'username' in session:
        if request.method == 'POST':
            content = request.get_json(silent=True)
            mesg = update_mouse(content,mouse_id)
            resp = Response(response=mesg, status=200, mimetype="application/json")
            return resp
        else:
            resp = Response(response=get_all_blood_hows(), status=200, mimetype="application/json")
            return resp
    else:
        return '403', 403

if __name__ == "__main__":
    app.secret_key = settings_secret_key
    app.debug = settings_debug
    app.run(host='0.0.0.0')
