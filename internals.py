from sql import alcsession, Cage, Color, Sex, Diet, Status, Study, Strain, Surgeon
from sql import Breed, Pool, Cause, Gather_Method, Gather_How, Mouse

import json
from datetime import datetime

from sqlalchemy.orm.exc import NoResultFound


def get_all_cages():
    cages = alcsession.query(Cage).all()

    cage_list = []
    for cage in cages:
        cage_list.append(cage.name)
    return json.dumps(cage_list)


def add_cage_with_name(new_cage_name):
    new_cage = Cage()
    new_cage.name = new_cage_name
    alcsession.add(new_cage)
    alcsession.commit()


def get_all_colors():
    colors = alcsession.query(Color).all()

    color_list = []
    for color in colors:
        color_list.append(color.color)
    return json.dumps(color_list)


def add_color(new_color_name):
    new_color = Color()
    new_color.color = new_color_name
    alcsession.add(new_color)
    alcsession.commit()


def get_all_sexes():
    sexes = alcsession.query(Sex).all()

    sex_list = []
    for sex in sexes:
        sex_list.append(sex.sex)
    return json.dumps(sex_list)


def add_sex(new_sex_name):
    new_sex = Sex()
    new_sex.sex = new_sex_name
    alcsession.add(new_sex)
    alcsession.commit()


def get_all_diets():
    diets = alcsession.query(Diet).all()

    diet_list = []
    for diet in diets:
        diet_list.append(diet.diet)
    return json.dumps(diet_list)


def add_diet(new_diet_name):
    new_diet = Diet()
    new_diet.diet = new_diet_name
    alcsession.add(new_diet)
    alcsession.commit()


def get_all_statuses():
    statuses = alcsession.query(Status).all()

    status_list = []
    for status in statuses:
        status_list.append(status.status)
    return json.dumps(status_list)


def add_status(new_status_name):
    new_status = Status()
    new_status.status = new_status_name
    alcsession.add(new_status)
    alcsession.commit()


def get_all_studies():
    studies = alcsession.query(Study).all()

    study_list = []
    for study in studies:
        study_list.append(study.study)
    return json.dumps(study_list)


def add_study(new_study_name):
    new_study = Study()
    new_study.study = new_study_name
    alcsession.add(new_study)
    alcsession.commit()


def get_all_strains():
    strains = alcsession.query(Strain).all()

    strain_list = []
    for strain in strains:
        strain_list.append(strain.strain)
    return json.dumps(strain_list)


def add_strain(new_strain_name):
    new_strain = Strain()
    new_strain.strain = new_strain_name
    alcsession.add(new_strain)
    alcsession.commit()


def get_all_breeds():
    breeds = alcsession.query(Breed).all()

    breed_list = []
    for breed in breeds:
        breed_list.append(breed.breed)
    return json.dumps(breed_list)


def add_breed(new_breed_name):
    new_breed = Breed()
    new_breed.breed = new_breed_name
    alcsession.add(new_breed)
    alcsession.commit()


def get_all_pools():
    pools = alcsession.query(Pool).all()

    pool_list = []
    for pool in pools:
        pool_list.append(pool.pool)
    return json.dumps(pool_list)


def add_pool(new_pool_name):
    new_pool = Pool()
    new_pool.pool = new_pool_name
    alcsession.add(new_pool)
    alcsession.commit()


def get_all_causes_of_death():
    causes_of_death = alcsession.query(Cause).all()

    cause_of_death_list = []
    for cause_of_death in causes_of_death:
        cause_of_death_list.append(cause_of_death.cause)
    return json.dumps(cause_of_death_list)


def add_cause_of_death(new_cause_of_death_name):
    new_cause_of_death = Cause()
    new_cause_of_death.cause = new_cause_of_death_name
    alcsession.add(new_cause_of_death)
    alcsession.commit()


def get_all_blood_gather_methods():
    blood_gather_methods = alcsession.query(Gather_Method).all()

    blood_gather_method_list = []
    for blood_gather_method in blood_gather_methods:
        blood_gather_method_list.append(blood_gather_method.gather_method)
    return json.dumps(blood_gather_method_list)


def add_blood_gather_method(new_blood_gather_method_name):
    new_blood_gather_method = Gather_Method()
    new_blood_gather_method.gather_method = new_blood_gather_method_name
    alcsession.add(new_blood_gather_method)
    alcsession.commit()


def get_all_blood_hows():
    blood_hows = alcsession.query(Gather_How).all()

    blood_how_list = []
    for blood_how in blood_hows:
        blood_how_list.append(blood_how.gather_how)
    return json.dumps(blood_how_list)


def add_blood_how(new_blood_how_name):
    new_blood_how = Gather_How()
    new_blood_how.gather_how = new_blood_how_name
    alcsession.add(new_blood_how)
    alcsession.commit()


def get_all_surgeons():
    surgeons = alcsession.query(Surgeon).all()

    surgeon_list = []
    for surgeon in surgeons:
        surgeon_list.append(surgeon.surgeon)
    return json.dumps(surgeon_list)


def add_surgeon(new_surgeon_name):
    new_surgeon = Surgeon()
    new_surgeon.surgeon = new_surgeon_name
    alcsession.add(new_surgeon)
    alcsession.commit()


def add_mouse(mouse_json_obj):

    errors = []
    mouse_in_question = Mouse()

    for trait in mouse_json_obj:

        # We make things easier to type first off..
        name = trait['name']
        value = trait['value']

        # If the line is empty then we pass first...
        # If a user passes in an invalid field that is empty we won't get an error but who cares.
        # Then we do the rest.
        if value == '':
            pass
        elif name == 'tag_number':
            mouse_in_question.tag_number = value
        elif name == 'parent1':
            try:
                parent_mouse = alcsession.query(Mouse).filter(Mouse.mouse_id == value).one()
                mouse_in_question.parent1 = parent_mouse.mouse_id
            except NoResultFound:
                errors.append('Parent mouse 1 with id %s does not exist. Did you add it first?' % value)
        elif name == 'parent2':
            try:
                parent_mouse = alcsession.query(Mouse).filter(Mouse.mouse_id == value).one()
                mouse_in_question.parent2 = parent_mouse.mouse_id
            except NoResultFound:
                errors.append('Parent mouse 2 with id %s does not exist. Did you add it first?' % value)
        elif name == 'cage':
            try:
                cage = alcsession.query(Cage).filter(Cage.name == value).one()
                mouse_in_question._cage_id = cage.cage_id
            except NoResultFound:
                errors.append('\'%s\' is not a valid cage. Did you add it as a constraint first?' % value)
        elif name == 'sex':
            try:
                sex = alcsession.query(Sex).filter(Sex.sex == value).one()
                mouse_in_question._sex_id = sex.sex_id
            except NoResultFound:
                errors.append('\'%s\' is not a valid sex. Did you add it as a constraint first?' % value)
        elif name == 'color':
            try:
                color = alcsession.query(Color).filter(Color.color == value).one()
                mouse_in_question._color_id = color.color_id
            except NoResultFound:
                errors.append('\'%s\' is not a valid color. Did you add it as a constraint first?' % value)
        elif name == 'diet':
            try:
                diet = alcsession.query(Diet).filter(Diet.diet == value).one()
                mouse_in_question._diet_id = diet.diet_id
            except NoResultFound:
                errors.append('\'%s\' is not a valid color. Did you add it as a constraint first?' % value)
        elif name == 'strain':
            try:
                strain = alcsession.query(Strain).filter(Strain.strain == value).one()
                mouse_in_question._strain_id = strain.strain_id
            except NoResultFound:
                errors.append('\'%s\' is not a valid strain. Did you add it as a constraint first?' % value)
        elif name == 'breed':
            try:
                breed = alcsession.query(Breed).filter(Breed.breed == value).one()
                mouse_in_question._breed_id = breed.breed_id
            except NoResultFound:
                errors.append('\'%s\' is not a valid breed. Did you add it as a constraint first?' % value)
        elif name == 'status':
            try:
                status = alcsession.query(Status).filter(Status.status == value).one()
                mouse_in_question._status_id = status.status_id
            except NoResultFound:
                errors.append('\'%s\' is not a valid status. Did you add it as a constraint first?' % value)
        elif name == 'study':
            try:
                study = alcsession.query(Study).filter(Study.study == value).one()
                mouse_in_question._study_id = study.study_id
            except NoResultFound:
                errors.append('\'%s\' is not a valid study. Did you add it as a constraint first?' % value)
        elif name == 'pool':
            try:
                pool = alcsession.query(Pool).filter(Pool.pool == value).one()
                mouse_in_question._pool_id = pool.pool_id
            except NoResultFound:
                errors.append('\'%s\' is not a valid pool. Did you add it as a constraint first?' % value)
        elif name == 'birth_date':
            date_object = datetime.strptime(value, '%m/%d/%Y')
            try:
                mouse_in_question.birth_date = date_object
            except Exception:
                errors.append('%s is not a valid format date time' % value)
        elif name == 'cause':
            try:
                cause = alcsession.query(Cause).filter(Cause.cause == value).one()
                mouse_in_question._cause_id = cause.cause_id
            except NoResultFound:
                errors.append('\'%s\' is not a valid cause of death. Did you add it as a constraint first?' % value)
        elif name == 'death_date':
            date_object = datetime.strptime(value, '%m/%d/%Y')
            try:
                mouse_in_question.death_date = date_object
            except Exception:
                errors.append('%s is not a valid format date time' % value)
        elif name == 'death_notes':
            mouse_in_question.death_note = value
        elif name == 'general_notes':
            mouse_in_question.general_note = value
        else:
            errors.append('%s is not a valid field' % name)

    if errors == []:
        alcsession.add(mouse_in_question)
        alcsession.commit()
        json_response = json.dumps({'status':'ok'})
    else:
        response = {'status':'error','errors':errors}
        json_response = json.dumps(response)

    return json_response


def update_mouse(mouse_json_obj,mouse_id):

    errors = []
    mouse_in_question = alcsession.query(Mouse).filter(Mouse.mouse_id == mouse_id).one()

    for trait in mouse_json_obj:

        # We make things easier to type first off..
        name = trait['name']
        value = trait['value']

        # If the line is empty then we pass first...
        # If a user passes in an invalid field that is empty we won't get an error but who cares.
        # Then we do the rest.
        if value == '':
            pass
        elif name == 'tag_number':
            mouse_in_question.tag_number = value
        elif name == 'parent1':
            try:
                parent_mouse = alcsession.query(Mouse).filter(Mouse.mouse_id == value).one()
                mouse_in_question.parent1 = parent_mouse.mouse_id
            except NoResultFound:
                errors.append('Parent mouse 1 with id %s does not exist. Did you add it first?' % value)
        elif name == 'parent2':
            try:
                parent_mouse = alcsession.query(Mouse).filter(Mouse.mouse_id == value).one()
                mouse_in_question.parent2 = parent_mouse.mouse_id
            except NoResultFound:
                errors.append('Parent mouse 2 with id %s does not exist. Did you add it first?' % value)
        elif name == 'cage':
            try:
                cage = alcsession.query(Cage).filter(Cage.name == value).one()
                mouse_in_question._cage_id = cage.cage_id
            except NoResultFound:
                errors.append('\'%s\' is not a valid cage. Did you add it as a constraint first?' % value)
        elif name == 'sex':
            try:
                sex = alcsession.query(Sex).filter(Sex.sex == value).one()
                mouse_in_question._sex_id = sex.sex_id
            except NoResultFound:
                errors.append('\'%s\' is not a valid sex. Did you add it as a constraint first?' % value)
        elif name == 'color':
            try:
                color = alcsession.query(Color).filter(Color.color == value).one()
                mouse_in_question._color_id = color.color_id
            except NoResultFound:
                errors.append('\'%s\' is not a valid color. Did you add it as a constraint first?' % value)
        elif name == 'diet':
            try:
                diet = alcsession.query(Diet).filter(Diet.diet == value).one()
                mouse_in_question._diet_id = diet.diet_id
            except NoResultFound:
                errors.append('\'%s\' is not a valid color. Did you add it as a constraint first?' % value)
        elif name == 'strain':
            try:
                strain = alcsession.query(Strain).filter(Strain.strain == value).one()
                mouse_in_question._strain_id = strain.strain_id
            except NoResultFound:
                errors.append('\'%s\' is not a valid strain. Did you add it as a constraint first?' % value)
        elif name == 'breed':
            try:
                breed = alcsession.query(Breed).filter(Breed.breed == value).one()
                mouse_in_question._breed_id = breed.breed_id
            except NoResultFound:
                errors.append('\'%s\' is not a valid breed. Did you add it as a constraint first?' % value)
        elif name == 'status':
            try:
                status = alcsession.query(Status).filter(Status.status == value).one()
                mouse_in_question._status_id = status.status_id
            except NoResultFound:
                errors.append('\'%s\' is not a valid status. Did you add it as a constraint first?' % value)
        elif name == 'study':
            try:
                study = alcsession.query(Study).filter(Study.study == value).one()
                mouse_in_question._study_id = study.study_id
            except NoResultFound:
                errors.append('\'%s\' is not a valid study. Did you add it as a constraint first?' % value)
        elif name == 'pool':
            try:
                pool = alcsession.query(Pool).filter(Pool.pool == value).one()
                mouse_in_question._pool_id = pool.pool_id
            except NoResultFound:
                errors.append('\'%s\' is not a valid pool. Did you add it as a constraint first?' % value)
        elif name == 'cause':
            try:
                cause = alcsession.query(Cause).filter(Cause.cause == value).one()
                mouse_in_question._cause_id = cause.cause_id
            except NoResultFound:
                errors.append('\'%s\' is not a valid cause of death. Did you add it as a constraint first?' % value)
        elif name == 'death_date':
            date_object = datetime.strptime(value, '%m/%d/%Y')
            try:
                mouse_in_question.death_date = date_object
            except Exception:
                errors.append('%s is not a valid format date time' % value)
        elif name == 'death_notes':
            mouse_in_question.death_note = value
        elif name == 'general_notes':
            mouse_in_question.general_note = value
        else:
            errors.append('%s is not a valid field' % name)

    if errors == []:
        alcsession.add(mouse_in_question)
        alcsession.commit()
        json_response = json.dumps({'status':'ok'})
    else:
        response = {'status':'error','errors':errors}
        json_response = json.dumps(response)

    return json_response

def find_mice(criteria_list):

    mouse_blob = alcsession.query(Mouse)

    for criterion in criteria_list:

        criteria = criterion['criteria']
        value = criterion['value']

        if criteria == "cages":
            mouse_blob = mouse_blob.filter(Mouse.cage.has(name = value))
        elif criteria == "colors":
            mouse_blob = mouse_blob.filter(Mouse.color.has(color = value))
        elif criteria == "sexes":
            mouse_blob = mouse_blob.filter(Mouse.sex.has(sex = value))
        elif criteria == "diets":
            mouse_blob = mouse_blob.filter(Mouse.diet.has(diet=value))
        elif criteria == "statuses":
            mouse_blob = mouse_blob.filter(Mouse.status.has(status=value))
        elif criteria == "studies":
            mouse_blob = mouse_blob.filter(Mouse.study.has(study=value))
        elif criteria == "strains":
            mouse_blob = mouse_blob.filter(Mouse.strain.has(strain=value))
        elif criteria == "breeds":
            mouse_blob = mouse_blob.filter(Mouse.breed.has(breed=value))
        elif criteria == "pools":
            mouse_blob = mouse_blob.filter(Mouse.pool.has(pool=value))
        elif criteria == "causes_of_death":
            mouse_blob = mouse_blob.filter(Mouse.cause.has(cause=value))
        else:
            pass

    remaining_mice = mouse_blob.all()
    return remaining_mice


def get_mouse(mouse_id):
    mouse = alcsession.query(Mouse).filter(Mouse.mouse_id == mouse_id).one()

    if mouse is None:
        return Mouse()
    else:
        return mouse


def generate_cage_card_from_mouse_blob(mouse_blob):
    cards = {}

    if len(mouse_blob) == 0:
        return {}

    for mouse in mouse_blob:
        if mouse.cage.name not in cards:
            cards[mouse.cage.name] = {}
            cards[mouse.cage.name]['cage_name'] = mouse.cage.name
            cards[mouse.cage.name]['birthdates'] = []
            cards[mouse.cage.name]['strain'] = None
            cards[mouse.cage.name]['study'] = None
            cards[mouse.cage.name]['sex'] = None
            cards[mouse.cage.name]['mouse_tags'] = []

        cards[mouse.cage.name]['birthdates'].append(mouse.birth_date_str())
        cards[mouse.cage.name]['strain'] = mouse.study_str()
        cards[mouse.cage.name]['study'] = mouse.study_str()
        cards[mouse.cage.name]['sex'] = mouse.sex_str()
        cards[mouse.cage.name]['mouse_tags'].append(mouse.tag_number)

    return cards






